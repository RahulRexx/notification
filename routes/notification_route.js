module.exports = {
    configure: function (app, mongo, ObjectID, url, assert, dbb) {
        var notification_module = require('../components/notification_component')(mongo, ObjectID, url, assert, dbb);


        // add notification
        app.post('/add_notification', function (req, res) {

            try {
                var new_notification = {
                    notification_title: req.body.notification_title,
                    notification_department: req.body.notification_department,
                    notification_validity: req.body.notification_validity,
                    notification_summary: req.body.notification_summary
                };
                blog_module.add_notification(new_notification, function (result, error, message) {
                    if (error) {
                        res.json({
                            status: false,
                            message: message
                        });
                    } else {
                        res.json({
                            status: true,
                            message: message
                        });
                    }
                })

            } catch (er) {
                console.log("error occures: " + er);
                res.json({
                    status: false,
                    message: "failed at try block...!"
                });
            }
        });


    }
}